#include <iostream>
//#include "character.hh"

class Character{
    private:
    int attack;
    int health_points;
    int energy_points;
    public:
    Character (int atk, int en, int hp){
        attack = atk;
        energy_points = en;
        health_points = hp;
    }
    int health(){
        return health_points;
    }
    void health(int x){
        health_points -= x;
    }
    void energy(int x){
        energy_points -= x;
    }
    int energy(){
        return energy_points;
    }
    int g_attack(){
        return attack;
    }
};

Character hero(35, 80, 50);
Character monster(30, 60, 55);

void kick(Character &x, Character &y){
    x.energy(5);
    y.health(x.g_attack() * 0.15);
}

int main(){
    kick(hero, monster);
    std::cout << monster.health() << '\n';
}