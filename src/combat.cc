#include "character.hh"
#include "combat.hh"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <iomanip>
#include <random>
#include <cstdlib>

// Actions

void Combat::run_battle()
{
    if (monsters.size() == 1)
    {
        std::cout << "\nA wild " << monsters[0].get_id() << " has appeared.\n";
    }
    else
    {
        std::cout << "\nA horde of " << monsters.size() << " monsters has appeared.\n";
    }

    auto is_alive = [](const Character &c)
    { return c.is_alive(); };
    while (std::any_of(scavengers.cbegin(), scavengers.cend(), is_alive) && std::any_of(monsters.cbegin(), monsters.cend(), is_alive) && state == CombatState::Running)
    {

        for (auto &scavenger : scavengers)
        {
            choose_action(scavenger);
        }

        for (auto &monster : monsters) {
            monster.choose_action(scavengers);
        }

        std::cout << "press any key to continue\n";
        std::cin >> std::ws;
        std::cin.get();
#ifdef _WIN64
        std::system("cls");
#else
        std::system ("clear");
#endif

        std::cout << '\n'
            << "╔════════════════════════════════════════════╗\n";
        for (auto &scavenger : scavengers) {
            scavenger.health_left();
            scavenger.release_shield();
        }
        std::cout << "║════════════════════════════════════════════║\n";
        for (auto &monster : monsters) {
            monster.health_left();
            monster.release_shield();
        }
        std::cout << "╚════════════════════════════════════════════╝\n";

        monsters.erase(std::remove_if(monsters.begin(), monsters.end(), [](const auto &m)
                    { return !m.is_alive(); }),
                monsters.end());
        scavengers.erase(std::remove_if(scavengers.begin(), scavengers.end(), [](const auto &s)
                    { return !s.is_alive(); }),
                scavengers.end());

    }

    state = std::any_of(scavengers.cbegin(), scavengers.cend(), is_alive) ? CombatState::Victory : CombatState::Defeat;
}

void Combat::save()
{
    savefile << scavengers.size() << '\n';
    for (const auto &scavenger : scavengers)
        savefile << scavenger << '\n';
    savefile << '\n' << monsters.size() << '\n';
    for (const auto &monster : monsters)
        savefile << monster << '\n';
    savefile.close();
}

void Combat::quit()
{
    switch (state) {
    case CombatState::Flight:
        std::cout << "The party fled.\n"
            << "\"The sin is not in being outmatched, but in failing to recognize it.\"";
    break;

    case CombatState::Defeat:
        std::cout << "Better luck next time.\n"
            << "\"Regroup. Reassemble. Evil is timeless, after all.\"\n";
    break;

    case CombatState::Running:
        std::cout << "The game was saved.\n"
            << "\"Death is patient, it will wait.\"\n";
        save();
    break;

    case CombatState::Victory:
        std::cout << "The party emerges triumphant!\n"
            << "\"Remind yourself that overconfidence is a slow and insidious killer.\"";
    break;
    }
    std::exit(0);
}

// Interface to Character

void Combat::choose_action(Scavenger &scavenger)
{
    std::cout << "\nWhat will " << scavenger.get_id() << " do?\n"
        << "[1] Attack\n"
        << "[2] Special\n"
        << "[3] Run\n"
        << "[4] Save and Quit\n";
    std::size_t choice;
    std::cin >> choice;
    switch (choice)
    {
    case 1:
        choose_target(scavenger);
    break;
    case 2:
        scavenger.special_action();
    break;
    case 3:
        state = CombatState::Flight;
        quit();
    break;
    case 4:
        quit();
    break;
    default:
        std::cout << "Input a valid option\n";
        choose_action(scavenger);
    }
}

void Combat::choose_target(Scavenger &scavenger)
{
    std::size_t choice;
    if (monsters.size() > 1)
    {
        std::cout << "\nWhich enemy will " << scavenger.get_id() << " target?\n";
        for (std::size_t i = 0; i < monsters.size(); ++i)
        {
            std::cout << "[" << (i + 1) << "] " << monsters[i].get_id() << '\n';
        }
        std::cin >> choice;
    }
    else
    {
        choice = 1;
    }
    std::cout << std::setw(16) << std::left << std::setfill('.') << scavenger.get_id() << " used "
        << scavenger.attack(monsters[choice - 1], Attack::Basic)
        <<" while targetting the " << monsters[choice - 1].get_id()
        << ".\n";
}

void Monster::choose_action(std::vector<Scavenger> &scavengers)
{
    bool attacked = false;
    static std::random_device rng;
    static std::mt19937 mersenne_twister(rng());
    if (behavior == Behavior::Violent) {
        std::discrete_distribution<int> d({9, 1});
        if (d(mersenne_twister) == 0) {
            attacked = true;
        } else {
            shield_up(stats.defense);
        }

    } else if (behavior == Behavior::Passive) {
        std::discrete_distribution<> d({1, 1});
        if (d(mersenne_twister) == 0) {
            attacked = true;
        } else {
            shield_up(stats.defense);
        }
    } else if (behavior == Behavior::Fearful) {
        if (stats.health <= stats.max_health/2) {
            std::discrete_distribution<> d({1, 2});
            if (d(mersenne_twister) == 0) {
                attacked = true;
            } else {
                shield_up(stats.defense);
            }
        } else {
            std::discrete_distribution<> d({1, 1});
            if (d(mersenne_twister) == 0) {
                attacked = true;
            } else {
                shield_up(stats.defense);
            }
        }
    } else if (behavior == Behavior::Berserker) {
        double modifier = 1.7 - 1.4*(stats.health/stats.max_health);
        double n = std::min(1.0, modifier);
        std::discrete_distribution<> d({n, 1 - n});
        if (d(mersenne_twister) == 0) {
            attacked = true;
        } else {
            shield_up(stats.defense);
        }
    }

    if(attacked){
        auto &target = choose_target(scavengers);
        std::cout << std::setw(16) << std::left << std::setfill('.') << get_id() << " used " << attack(target, Attack::Charge)
            << " while targetting " << target.get_id() << "\n";
    }
}
