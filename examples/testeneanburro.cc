#include <iostream>
//#include "character.hh"

class Character{
    private:
    int attack_points;
    int health_points;
    public:
    Character (int atk, int hp){
        attack_points = atk;
        health_points = hp;
    }
    int health(){
        return health_points;
    }
    void health(int x){
        health_points -= x;
    }
    int get_attack_points(){
        return attack_points;
    }
};

Character hero(35, 50);
Character monster(30, 55);

void kick(Character &scavenger, Character &monster){
    monster.health(scavenger.get_attack_points());
}

int main(){
    kick(hero, monster);
    std::cout << monster.health() << '\n';
}
