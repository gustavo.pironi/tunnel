#include "character.hh"
#include "combat.hh"
#include <filesystem>
#include <fstream>
#include <iostream>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " <savefile>\n";
        return 1;
    }

    std::cout << "╔═══════════════════════════════╗\n"
              << "║                               ║\n"
              << "║    The Under Scrolls IIIII    ║\n"
              << "║                               ║\n"
              << "╚═══════════════════════════════╝\n"
              << "Welcome to the Tunnel!\n";

    std::vector<Scavenger> scavengers;
    std::vector<Monster> monsters;
    load_presets();

    if (std::filesystem::exists(argv[1]) && !std::filesystem::is_empty(argv[1])) {
        std::ifstream savefile(argv[1]);
        scavengers = Scavenger::populate(savefile);
        monsters = Monster::populate(savefile);
    } else {
        std::cout << "How large is your party?\n";
        int party_size;
        std::cin >> party_size;
        scavengers = Scavenger::make_party(party_size);

        std::size_t difficulty;
        do {
            std::cout << "\nChoose the difficulty:\n[1] Easy\n[2] Normal\n[3] Hard\n";
            std::cin >> difficulty;
        } while (difficulty < 1 && difficulty > 3);
        monsters = Monster::make_party(party_size, difficulty);
    }

    Combat combat(scavengers, monsters, argv[1]);
    combat.run_battle();
}
