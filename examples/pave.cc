#include <iostream>
#include <vector>
#include <array>

constexpr int n_type_monster = 13;
std::array<Monster, n_type_monster> monsters = {GiantWorm, Slime, CrazyMole, FakePlant, Hungry, EtherealBug,
PoisonousSpider, GiantSpider, QueenSpider, CorruptedGunner, CorruptedTank, AshCrawler, AbandonedRobot};
std::vector<Monster> monsters_round;
std::vector<Monster> chosen_monsters;

int probability(float chance1, float chance2, float chance3){
    float n;
    static std::random_device dev;
    static std::mt19937 gen(dev());
    static std::uniform_real_distribution<double> dis(0.0,1.0);
    n = dis(gen);
    int answer;
    if (n <= chance1){
        answer = 1;
    } else if (n <= chance1 + chance2){
        answer = 2;
    }else if (n <= chance1 + chance2 + chance3){
        answer = 3;
    }else {
        answer = 4;
    }
    std::cout << answer << '\n';
    return answer;
}

void choose_enemies (int current_floor){
    static int difficulty_floor;
    int difficulty_points = 0;
    difficulty_floor = current_floor + 3;
    for (int i = 0; i < n_type_monster; ++i){
        if (current_floor >= monsters[i].status.entry && current_floor <= monsters[i].status.exit){
            monsters_round.push_back(monsters[i]);
        }
    }
    static float first_option;
    static float second_option;
    static float third_option;
    if (monsters_round.size() == 1){
        first_option = 1.0;
        second_option = 0.0;
        third_option = 0.0;
    }else if (monsters_round.size() == 2){
        first_option = 0.6;
        second_option = 0.4;
        third_option = 0.0;
    }else if (monsters_round.size() == 3){
        first_option = 0.35;
        second_option = 0.5;
        third_option = 0.15;
    }else{
        first_option = 0.15;
        second_option = 0.45;
        third_option = 0.3;
    }
    while (true){
        int chosen_enemy = probability(first_option, second_option, third_option);
        difficulty_points += monsters_round[chosen_enemy - 1].status.level;
        chosen_monsters.push_back(monsters_round[chosen_enemy - 1]);
        if (difficulty_points >= difficulty_floor){
            break;
        }
    }
}

