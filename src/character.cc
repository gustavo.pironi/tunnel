#include "character.hh"
#include "combat.hh"
#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <random>
#include <iomanip>


/// Character
std::string Character::generate_phrases(QuoteType quote_type)
{
    std::vector<std::string> quotes;
    switch (quote_type)
    {
    case QuoteType::Heal:
        quotes = {
            "\"Soothed, sedated.\"",
            "\"A momentary abatement...\"",
            "\"The wounds of war can be healed, but never hidden.\"",
            "\"Death cannot be escaped. But it can be postponed!\"",
            "\"The flesh is knit!\"",};
    break;
    case QuoteType::Attack:
        quotes = {
            "\"The slow death - unforeseen, unforgiving.\"",
            "\"A time to perform beyond one's limits!",
            "\"Precision and power!\"",
            "\"Press this advantage, give them no quarter!\"",
            "\"Their formation will break - maintain the offensive!\""};
    break;

    }
    static std::uniform_int_distribution<std::size_t> generate_index(0, quotes.size() - 1);
    static std::random_device rng;
    static std::mt19937 mersenne_twister(rng());

    return quotes[generate_index(mersenne_twister)];
}

// Actions

std::string Character::attack(Character &target, Attack atk)
{
    double modifier = 0;
    std::string id;
    switch(atk) {
    case Attack::Basic:
        modifier = 1;
        id = "Basic Attack";
    break;

    case Attack::Stab:
        modifier = 1;
        id = "Stab";
    break;

    case Attack::Charge:
        modifier = 1;
        id = "Charge";
    break;
    }

    if (is_alive())
        target.change_health(modifier * stats.attack);
    return id;
}

void Character::release_shield()
{
    stats.shield = 0;
}

void Character::shield_up(const int sp)
{
    stats.shield += sp;
}

void Character::sniper_shot()
{
    stats.attack *= 1.4;
    std::cout << get_id() << " Oils his gun... Attack raised by 40%\n"
        << generate_phrases(QuoteType::Attack) << '\n';
}

void Character::heal(const int hp)
{
    if(stats.health + hp >= stats.max_health){
        std::cout << get_id() << " healed himself and restored " << stats.max_health - stats.health << " health points.\n";
        stats.health = stats.max_health;
    }
    else{
        stats.health += hp;
        std::cout << get_id() << " healed himself and restored " << hp << " health points.\n";
    }
    std::cout << generate_phrases(QuoteType::Heal) << std::endl;

}

void Scavenger::special_action()
{
    if (profession == Profession::Alchemist) {
        heal(20);
    } else if (profession == Profession::Tank) {
        shield_up(30);
        std::cout << get_id() << " raised his shield!\n";
    } else if (profession == Profession::Gunner) {
        sniper_shot();
    }
}

void Character::change_health(int delta)
{
    if (stats.shield >= delta) {
        stats.shield -= delta;
        delta = 0;
    }
    if (stats.shield < delta) {
        delta -= stats.shield;
    }
    stats.health -= delta;
}

void Character::health_left() const
{
    if (stats.health > 0) {
        std::cout << "║  "<< std::setw(18) << stats.id << " has "
            << std::setw(11) << stats.health << "HP left ║\n";
    } else {
        std::cout << "║  "<< std::setw(18) << stats.id << " is no more.\n";
    }
}

// Operators

std::ostream& operator<<(std::ostream &os, const Stats &s)
{
    os << '\"' << s.id << "\" "
        << s.level << ' '
        << s.experience << ' '
        << s.max_health << ' '
        << s.health << ' '
        << s.attack << ' '
        << s.evasion << ' '
        << s.defense << ' '
        << s.energy << ' '
        << s.shield;
    return os;
}

std::istream& operator>>(std::istream &is, Stats &s)
{
    is.ignore(std::numeric_limits<std::streamsize>::max(), '\"');
    s.id="";
    for (char c; is.get(c) && c != '\"'; )
        s.id += c;
    is >> s.level
        >> s.experience
        >> s.max_health
        >> s.health
        >> s.defense
        >> s.attack
        >> s.evasion
        >> s.energy
        >> s.shield;
    return is;
}

/// Scavenger

// Constructors and alike

std::string ordinal(unsigned n) {
    switch(n) {
    case 1:
        return "first";
    case 2:
        return "second";
    case 3:
        return "third";
    }
    std::terminate();
}

Scavenger::Scavenger(Profession p, std::string name)
{
    *this = presets.at(static_cast<unsigned>(p));
    stats.id = name;
}

std::vector<Scavenger> Scavenger::make_party(int &party_size)
{
    std::vector<Scavenger> scavengers;

    if (party_size > 3) {
        std::cout << "It won't fit the tunnel. " << party_size << " scavengers is too much. You will have to make do with 3.\n";
        party_size = 3;
    }

    if (party_size <= 0) {
        std::cout << "How brave! Let's see how it goes.\n";
        --party_size;
    }

    for (int i = 0; i < party_size; ++i) {
        std::string name;
        std::cout << "\nWhat's the name of the " << ordinal(i + 1) << " scavenger?\n";
        std::cin >> std::ws;
        std::getline(std::cin, name);

        std::size_t choice;
        std::cout << "\nWhat's " << name << "'s profession?\n"
            << "[1] Gunner\n"
            << "[2] Alchemist\n"
            << "[3] Tank\n";
        std::cin >> choice;

        if (choice < 1 || choice > 3) {
            continue;
        }

        Profession profession;
        switch (choice) {
        case 1:
            profession = Profession::Gunner;
            std::cout << "You chose the Gunner!\n";
        break;

        case 2:
            profession = Profession::Alchemist;
            std::cout << "You chose the Alchemist!\n";
        break;

        case 3:
            profession = Profession::Tank;
            std::cout << "You chose the Tank!\n";
        break;
        }
        scavengers.emplace_back(profession, name);
    }

    return scavengers;
}

std::vector<Scavenger> Scavenger::populate(std::istream &savefile)
{
    std::size_t party_size;
    savefile >> party_size;

    std::vector<Scavenger> scavengers;
    scavengers.reserve(party_size);

    for (std::size_t i = 0; i < party_size; ++i) {
        Scavenger scavenger;
        savefile >> scavenger;
        scavengers.push_back(scavenger);
    }

    return scavengers;
}

// Operators

std::ostream& operator<<(std::ostream &os, const Scavenger &s)
{
    os << static_cast<unsigned>(s.profession) << ' '
        << s.stats;
    return os;
}

std::istream& operator>>(std::istream &is, Scavenger &m)
{
    unsigned p;
    is >> p;
    m.profession = static_cast<Scavenger::Profession>(p);
    is >> m.stats;
    return is;
}

/// Monster

// Constructors and alike

Monster::Monster(Type t)
{
    *this = presets.at(static_cast<unsigned>(t));
}

std::vector<Monster> Monster::populate(std::istream &savefile)
{
    std::size_t horde_size;
    savefile >> horde_size;

    std::vector<Monster> monsters;
    monsters.reserve(horde_size);

    for (std::size_t i = 0; i < horde_size; ++i) {
        Monster monster;
        savefile >> monster;
        monsters.push_back(monster);
    }

    return monsters;
}


// NOTE: party_size is supposed to overflow if negative
std::vector<Monster> Monster::make_party(unsigned short party_size, std::size_t difficulty)
{
    std::vector<Monster> monsters;

    const auto points_cap = (50 + 25 * difficulty) * party_size;
    const auto [total_monsters, total_weak_monsters] = count_monster_presets();
    std::size_t current_points = 0;
    static std::random_device rng;
    static std::mt19937 mersenne_twister(rng());
    static std::uniform_int_distribution<std::size_t> index(0, total_monsters - 1);
    static std::uniform_int_distribution<std::size_t> index_weak(0, total_weak_monsters - 1);

    while(current_points < points_cap){
        monsters.emplace_back(static_cast<Type>(index(mersenne_twister)));
        current_points += monsters[monsters.size() - 1].get_points();
    }

    if(current_points > (points_cap + 50)){
        current_points -= monsters[monsters.size() - 1].get_points();
        monsters.erase(monsters.end());
        while(current_points < points_cap){
            monsters.emplace_back(static_cast<Type>(index_weak(mersenne_twister)));
            current_points += monsters[monsters.size() - 1].get_points();
        }
    }

    return monsters;
}

std::pair<std::size_t, std::size_t> Monster::count_monster_presets()
{
    return std::make_pair(Monster::presets.size(), std::count_if(Monster::presets.begin(), Monster::presets.end(), [](const auto &m){ return m.stats.level == 1; }));
}

void load_presets()
{
    std::ifstream is("data/monsters");
    std::string fields;
    std::getline(is, fields);
    Monster m;
    while(is >> m)
        Monster::presets.push_back(m);
    is.close();

    is.open("data/scavengers");
    std::getline(is, fields);
    Scavenger s;
    while(is >> s)
        Scavenger::presets.push_back(s);
}

// Actions

Scavenger& Monster::choose_target(std::vector<Scavenger> &scavengers)
{
    static std::random_device rng;
    static std::mt19937 mersenne_twister(rng());
    static std::uniform_int_distribution<std::size_t> index(0, scavengers.size() - 1);
    static std::uniform_int_distribution<std::size_t> attack_mode(1, 2);
    switch (attack_mode(mersenne_twister))
    {
    case 1:
        return scavengers[index(mersenne_twister)];
    break;

    case 2:
        return *std::min_element(scavengers.begin(), scavengers.end(), [](const auto c1, const auto c2){ return c1.get_health() < c2.get_health(); });
    break;
    }
    std::terminate();
}

// Operators

std::ostream& operator<<(std::ostream &os, const Monster &m)
{
    os << static_cast<unsigned>(m.type) << ' '
        << static_cast<unsigned>(m.behavior) << ' '
        << m.stats;
    return os;
}

std::istream& operator>>(std::istream &is, Monster &m)
{
    unsigned n;
    is >> n;
    m.type = static_cast<Monster::Type>(n);

    is >> n;
    m.behavior = static_cast<Monster::Behavior>(n);

    is >> m.stats;
    return is;
}
