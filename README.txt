╔═══════════════════════════════╗
║                               ║
║    The Under Scrolls IIIII    ║
║                               ║
╟───────────────────────────────╢
║Felipe Masao Miura             ║
║Gustavo Dal Evedove Pironi     ║
║Gustavo Diniz Oliveira da Hora ║
║Nean Segura                    ║
╚═══════════════════════════════╝

Instructions
* To compile: `make`
* To run: `./tunnel <savefile>`
* If `<savefile>` is empty, you will be prompted to create your scavenger party,
  otherwise the battle will start immediately
* Follow the instructions shown on screen by typing the number inside the
  square brackets of the chosen option
* Your scavenger party can have as many integrants as you please (or your
  system allow, whichever come first).
* On the other side the enemy horde will be decided by the chosen difficulty
