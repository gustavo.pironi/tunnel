#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <random>
#include <ctime>

class Item{
    public:
    private:
    int value;
    int bonus;
};

Item spear;
Item collar;
Item bat_wing;
Item bat_teeth;
Item scorpion_stinger;
Item common_poison;

std::vector<Item> bag;

class Ring : Item{
    public:

    private:

};

class Character{
    public:
    int get_health(){
        return health;
    }
    void set_health(int x){
        health -= x;
    }
    int get_attack_multiplier(){
        return attack_multiplier;
    }
    void set_energy(int y){
        energy -= y;
    }
    int get_movement(int x){
        return movement_character[x];
    }
    std::string get_name(){
        return name;
    }
    std::string print(Character &printed){
        std::cout << "Health: " << health << '\t'
        << "Attack: " << attack_multiplier << '\t'
        << "Energy: " << energy << '\n';
    }
    Character (std::string nm, int hp, int atk, int en, int ac, int lv, std::array<int, 4> mv_c): name(nm), health(hp), attack_multiplier(atk), energy(en), accuracy(ac), level(lv), movement_character(mv_c){}

    private:
    std::string name;
    int health;
    int attack_multiplier;
    int energy;
    int level;
    int accuracy;
    std::array<int, 4> movement_character;
};

class Scavenger : public Character{
    public:
    
    void set_experience (int xp){
        experience += xp;
    }

    using Character::Character;
    Scavenger (std::string nm, int hp, int atk, int en, int ac, int lv, int xp, std::array<int, 4> mv_c): Character(nm, hp, atk, en, ac, lv, mv_c){
        experience = xp;
    }
    private:
    int experience;
};

class Gunner : public Scavenger{
    public:
    using Scavenger::Scavenger;
    private:

};

class Monster : public Character{
    public:
    using Character::Character;
    Monster (std::string nm, int hp, int atk, int en, int ac, int lv, int dif, int e_fl, int l_fl, int c_of, int c_def, int c_dumb, Item i1, Item i2, std::array<int,4> mv_c): Character(nm, hp, atk, en, ac, lv, mv_c){
        difficulty = dif;
        enter_floor = e_fl;
        leave_floor = l_fl;
        item1 = i1;
        item2 = i2;
        chance_offensive = c_of;
        chance_deffensive = c_def;
        chance_dumb = c_dumb;
    }
    int get_enter_floor(){
        return enter_floor;
    }
    int get_leave_floor(){
        return leave_floor;
    }
    int get_difficulty(){
        return difficulty;
    }
    int get_chance_offensive(){
        return chance_offensive;
    }
    int get_chance_deffensive(){
        return chance_deffensive;
    }
    int get_chance_dumb(){
        return chance_dumb;
    }
    
    private:
    int difficulty;
    int enter_floor;
    int leave_floor;
    int chance_offensive;
    int chance_deffensive;
    int chance_dumb;
    Item item1;
    Item item2;
};

class Goblin : public Monster{
    public:
    using Monster::Monster;
    private:
};

class Giant_scorpion : public Monster{
    public:
    using Monster::Monster;
    private:
};

class Bat : public Monster{
    public:
    using Monster::Monster;
    private:
};

class Behavior{
    public:
    Behavior (float p_m1, float p_m2, float p_m3): probability_move1(p_m1), probability_move2(p_m2), probability_move3(p_m3){}
    private:
    float probability_move1;
    float probability_move2;
    float probability_move3;
};

Gunner bortolazzo ("Bortolazzo", 65, 37, 40, 35, 1, {0, 1, 2, 3});
Goblin goblin ("Goblin", 25, 12, 20, 30, 1, 15, 1, 9, 3, 1, 0, spear, collar, {0, 1, 2, 3});
Bat bat ("Bat", 12, 7, 20, 10, 1, 6, 1, 6, 1, 1, 2, bat_wing, bat_teeth, {0, 1, 2, 3});
Giant_scorpion giant_scorpion ("Giant Scorpion", 60, 47, 50, 22, 5, 40, 5, 15, 3, 0, 1, scorpion_stinger, common_poison, {0, 1, 2, 3});

int money = 0;
int turn = 0, action, difficulty_floor, difficulty_points = 0;
constexpr int n_type_monster = 3;

//void damage_poison(int damage_round){}

//std::vector<Monster> enemies = ;

std::array <Monster, n_type_monster> monsters = {goblin, bat, giant_scorpion};
std::vector<Monster> monsters_round;
std::vector<Monster> chosen_monsters;
std::vector<Behavior> monster_behavior;
std::vector<Scavenger> team = {bortolazzo, bortolazzo, bortolazzo};

//behavior(prob_attack1, prob_attack2, prob_def, prob_other)
Behavior agressive(0.1, 0.6, 0.25);
Behavior deffensive(0.05, 0.3, 0.5);
Behavior dumb(0.25, 0.25, 0.25);
Behavior fearful(0.3, 0.1, 0.25);
/*
float random(){
    std::time_t result = std::time(nullptr);;
    std::mt19937 gen(result);
    std::uniform_real_distribution<> dis(0.0,1.0);
    return dis(gen);
}
*/
std::time_t result = std::time(nullptr);
std::mt19937 gen(result);
std::uniform_real_distribution<> dis(0.0,1.0);

int probability (float chance1, float chance2, float chance3){
    float n;
    std::random_device dev;
    std::mt19937 gen(dev());
    std::uniform_real_distribution<> dis(0.0,1.0);
    n = dis(gen);
    int answer;
    if (n <= chance1){
        answer = 1;
    } else if (n <= chance1 + chance2){
        answer = 2;
    }else if (n <= chance1 + chance2 + chance3){
        answer = 3;
    }else {
        answer = 4;
    }
    return answer;
}
/*
void chose_enemies (int current_floor){
    difficulty_floor = current_floor * 25;
    for (int i = 0; i < n_type_monster; ++i){
        if (current_floor >= monsters[i].get_enter_floor() && current_floor <= monsters[i].get_leave_floor()){
            monsters_round.push_back(monsters[i]);
        }
    }
    float first_option;
    float second_option;
    float third_option;
    if (monsters_round.size() == 4){
        float first_option = 0.15;
        float second_option = 0.3;
        float third_option = 0.45;
    }else if (monsters_round.size() == 3){
        float first_option = 0.35;
        float second_option = 0.5;
        float third_option = 0.15;
    }else if (monsters_round.size() == 2){
        float first_option = 0.6;
        float second_option = 0.4;
        float third_option = 0.0;
    }else if (monsters_round.size() == 1){
        float first_option = 1.0;
        float second_option = 0.0;
        float third_option = 0.0;
    }
    while (true){
        int chosen_enemy = probability(dis(gen), first_option, second_option, third_option);
        //std::cout << chosen_enemy << '\n';
        difficulty_points += monsters[chosen_enemy].get_difficulty();
        chosen_monsters.push_back(monsters[chosen_enemy]);
        if (difficulty_points <= difficulty_floor && difficulty_points >= 0.75 * difficulty_floor){
            break;
        }else if (difficulty_points > difficulty_floor){
            chosen_monsters.clear();
        }
    }
    int n_behavior;
    for(int i = 0; i < monsters_round.size(); ++i){
        n_behavior = probability(dis(gen), 0.25, 0.25, 0.25);
        if (n_behavior < monsters_round[i].get_chance_offensive()){
            monster_behavior[i] = agressive;
        }else if(n_behavior < monsters_round[i].get_chance_offensive() + monsters_round[i].get_chance_deffensive()){
            monster_behavior[i] = deffensive;
        }else if(n_behavior < monsters_round[i].get_chance_offensive() + monsters_round[i].get_chance_deffensive() + monsters_round[i].get_chance_dumb()){
            monster_behavior[i] = dumb;
        }else {
            monster_behavior[i] = fearful;
        }
    }
}
*/

void punch (Character &a, Character &e){ //1
    e.set_health (a.get_attack_multiplier() * 0.20);
    a.set_energy(4);
}
void quick (Character &a, Character &e){ //2
    e.set_health (a.get_attack_multiplier() * 0.35);
    a.set_energy(9);
}
void life_absorb (Character &a, Character &e){ //3
    e.set_health (a.get_attack_multiplier() * 0.15);
    a.set_energy(8);
    a.set_health (a.get_attack_multiplier() * (-0.08));
}
void sting (Character &a, Character &e){ //4
    e.set_health (a.get_attack_multiplier() * 0.15);
    a.set_energy(8);
    //set_poison();
}

void chose_attack(int number_movement, Character& attacker, Character& target){
    if(number_movement == 1){
        punch (attacker, target);
    }else if(number_movement == 2){
        quick (attacker, target);
    }else if (number_movement == 3){
        life_absorb (attacker, target);
    }else if (number_movement == 4){
        sting (attacker, target);
    }
}
std::string print_movement(int n_movement){
    if(n_movement == 1)
        return "Punch";
    else if(n_movement == 2)
        return "Quick";
    else if(n_movement == 3)
        return "Life absorb";
    else if(n_movement == 4)
        return "Sting";
}

int main(){
    bool next_round = true, next_battle = true;
    for (int floor = 1; next_battle == true; ++floor){
        //chose_enemies(floor);
        difficulty_floor = floor * 25;
        for (int i = 0; i < n_type_monster; ++i){
            if (floor >= monsters[i].get_enter_floor() && floor <= monsters[i].get_leave_floor()){
                monsters_round.push_back(monsters[i]);
            }
        }
        float first_option;
        float second_option;
        float third_option;
        if (monsters_round.size() == 4){
            float first_option = 0.15;
            float second_option = 0.3;
            float third_option = 0.45;
        }else if (monsters_round.size() == 3){
            float first_option = 0.35;
            float second_option = 0.5;
            float third_option = 0.15;
        }else if (monsters_round.size() == 2){
            float first_option = 0.6;
            float second_option = 0.4;
            float third_option = 0.0;
        }else if (monsters_round.size() == 1){
            float first_option = 1.0;
            float second_option = 0.0;
            float third_option = 0.0;
        }
        while (true){
            int chosen_enemy = probability(first_option, second_option, third_option);
            std::cout << chosen_enemy << '\n';
            difficulty_points += monsters[chosen_enemy].get_difficulty();
            chosen_monsters.push_back(monsters[chosen_enemy]);
            if (difficulty_points <= difficulty_floor && difficulty_points >= 0.75 * difficulty_floor){
                break;
            }else if (difficulty_points > difficulty_floor){
                chosen_monsters.clear();
            }
        }
        int n_behavior;
        for(int i = 0; i < monsters_round.size(); ++i){
            n_behavior = probability(0.25, 0.25, 0.25);
            if (n_behavior < monsters_round[i].get_chance_offensive()){
                monster_behavior[i] = agressive;
            }else if(n_behavior < monsters_round[i].get_chance_offensive() + monsters_round[i].get_chance_deffensive()){
                monster_behavior[i] = deffensive;
            }else if(n_behavior < monsters_round[i].get_chance_offensive() + monsters_round[i].get_chance_deffensive() + monsters_round[i].get_chance_dumb()){
                monster_behavior[i] = dumb;
            }else {
                monster_behavior[i] = fearful;
            }
        }
        while (next_round == true){
            int hero_action = 0;
            int chosen_movement, chosen_target;
            for (; hero_action < team.size(); ++hero_action){
                std::cout << "Choose your action: [1] moves\t [2]item\t [3]run\n";
                std::cin >> action;
                if (action == 1){
                    std::cout << "Choose your movement: [1] " << print_movement(team[hero_action].get_movement(0) + 1)
                    << "\t [2] " << print_movement(team[hero_action].get_movement(1) + 1)
                    << "\t [3] " << print_movement(team[hero_action].get_movement(2) + 1)
                    << "\t [4] " << print_movement(team[hero_action].get_movement(3) + 1);
                    std::cout << '\n';
                    std::cin >> chosen_movement;
                    std::cout << "Choose your target: ";
                    std::cout << chosen_monsters.size() << '\n';

                    for (int i = 0; i < chosen_monsters.size(); ++i){
                        std::cout << "[" << i + 1 << "] " << chosen_monsters[i].get_name() << '\t';
                    }
                    std::cout << '\n';
                    std::cin >> chosen_target;
                    chose_attack(team[hero_action].get_movement(chosen_movement - 1), team[hero_action], chosen_monsters[chosen_target - 1]);
                }else if (action == 2){

                }else if (action == 3){

                }
            }
        }
    }
}
