#pragma once
#include <iostream>
#include <vector>

enum class Attack { Basic, Stab, Charge, };
enum class QuoteType { Heal, Attack};

enum class Faction { Scavenger, Monster };

struct Stats {
    std::string id;
    int level;
    int experience;
    int max_health;
    int health;
    int attack;
    double evasion;
    int defense;
    int energy;
    int shield;

    // Operators
    friend std::ostream& operator<<(std::ostream&, const Stats&);
    friend std::istream& operator>>(std::ostream&, const Stats&);
};

class Character {
public:
    // Actions
    std::string attack(Character&, Attack);
    void shield_up(const int);
    void release_shield();
    void heal(const int);
    void heal(Character&, const int);
    void sniper_shot();
    void lifesteal(Character&, const double, const double);
    void protect(Character&, const int, const int);
    void poison(Character&, const int, const int);

    // Combat auxiliaries
    void battle_text(Character&) const;
    void change_health(int);
    int get_health() const { return stats.health; }
    std::string get_id() const { return stats.id; }
    std::size_t get_points() const { return stats.experience; }
    void health_left() const;
    bool is_alive() const { return stats.health > 0; }
    std::string generate_phrases(QuoteType);
protected:
    Stats stats;
    void take_damage(Character&, const double);
};

class Scavenger : public Character {
public:
    enum class Profession : unsigned {
        Alchemist,
        Gunner,
        Tank,
        Geographer,
    };

    // Constructors and helpers
    Scavenger() = default;
    Scavenger(Profession, std::string);
    static std::vector<Scavenger> make_party(int&);
    static std::vector<Scavenger> populate(std::istream&);
    friend void load_presets();

    // Actions
    void special_action();

    // Operators
    friend std::ostream& operator<<(std::ostream&, const Scavenger&);
    friend std::istream& operator>>(std::istream&, Scavenger&);
private:
    Profession profession;

    inline static std::vector<Scavenger> presets;
};

class Monster : public Character {
public:
    enum class Type : unsigned {
        GiantWorm,
        Slime,
        CrazyMole,
        Ghoul,
        PoisonousSpider,
        EtherealBug,
        GiantSpider,
        CorruptedGunner,
        CorruptedTank,
        AshCrawler,
        AbandonedRobot,
        QueenSpider,
    };

    enum class Behavior : unsigned { Violent, Passive, Fearful, Berserker };

    // Constructors and helpers
    Monster() = default;
    Monster(Type);
    static std::vector<Monster> populate(std::istream&);
    static std::vector<Monster> make_party(unsigned short, std::size_t);
    friend void load_presets();
    static std::pair<std::size_t, std::size_t> count_monster_presets();

    // Actions
    void choose_action(std::vector<Scavenger>&);
    Scavenger& choose_target(std::vector<Scavenger>&);

    // Operators
    friend std::ostream& operator<<(std::ostream&, const Monster&);
    friend std::istream& operator>>(std::istream&, Monster&);
private:
    Type type;
    Behavior behavior;

    inline static std::vector<Monster> presets;
};

void load_presets();
