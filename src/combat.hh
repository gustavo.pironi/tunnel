#pragma once
#include "character.hh"
#include <fstream>
#include <vector>

enum class CombatState { Defeat, Flight, Running, Victory, };

class Combat {
public:
    // Constructors
    Combat(std::vector<Scavenger> &s, std::vector<Monster> &m, const char sf[]) : scavengers(s), monsters(m), savefile(sf) {};

    // Actions
    void run_battle();
    void save();
    void quit();

    // Interface to Character
    void choose_action(Scavenger&);
    void choose_attack(Scavenger&);
    void choose_target(Scavenger&);
    void set_back();

    // Operators
    friend std::ostream& operator<<(std::ostream&, const Combat&);
private:
    std::vector<Scavenger> scavengers;
    std::vector<Monster> monsters;
    std::ofstream savefile;
    std::size_t turn = 1;
    CombatState state = CombatState::Running;
};

    