#include <iostream>
#include <random>
#include <ctime>

int probability(float chance1, float chance2, float chance3){
    float n;
    static std::random_device dev;
    static std::mt19937 gen(dev());
    static std::uniform_real_distribution<double> dis(0.0,1.0);
    n = dis(gen);
    int answer;
    if (n <= chance1){
        answer = 1;
    } else if (n <= chance1 + chance2){
        answer = 2;
    }else if (n <= chance1 + chance2 + chance3){
        answer = 3;
    }else {
        answer = 4;
    }
    std::cout << answer << '\n';
    return answer;
}

int main(){
    for (int i = 0; i < 10; ++i){
        std::cout << probability(0.25,0.25,0.25) << ' ';

    }
}